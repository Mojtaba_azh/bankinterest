/**
 * This class is used for working with xml files (Singleton Pattern)
 *
 * @author Mojtaba Azhdari
 */
package dotin.utility;

import dotin.calculator.InterestCalculatorImp;
import dotin.deposit.Deposit;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.util.*;

public class XmlUtility {

    private static XmlUtility xmlUtility = new XmlUtility();

    // **************************************************
    // private Constructor
    // **************************************************
    private XmlUtility () {
    }

    // **************************************************************************
    // This public method is used for getting instance of this class.
    // **************************************************************************
    public static XmlUtility getInstance() {
        return xmlUtility;
    }

    // ********************************************************************************
    // This method get an XML input file,
    // parses into proper Objects based on the deposit Type value of every deposit
    // and returns all Objects as a list of Deposits ...
    // ********************************************************************************
    public List<Deposit> parse(String xmlInput) throws Exception {
        // **************************************************
        // Fields
        // **************************************************
        Map<String, String> exceptionMessages = ExceptionMessages.getInstance().getExceptionMessages();
        List<Deposit> deposits = new ArrayList<>();
        String depositType;
        BigDecimal depositBalance;
        String customerNumber;
        int durationInDays;

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
        File file = new File(xmlInput);
        Document document = documentBuilder.parse(file);
        document.getDocumentElement().normalize();
        NodeList nodeList = document.getElementsByTagName("deposit");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                try {
                    customerNumber = element.getElementsByTagName("customerNumber").item(0).getTextContent();
                    depositType = element.getElementsByTagName("depositType").item(0).getTextContent();
                    depositBalance = new BigDecimal(element.getElementsByTagName("depositBalance").item(0).getTextContent());
                    durationInDays = Integer.parseInt(element.getElementsByTagName("durationInDays").item(0).getTextContent());

                    if (!(customerNumber.chars().allMatch(Character::isDigit))) {
                        throw new Exception(exceptionMessages.get("customerNumber"));
                    }
                    if (durationInDays <= 0)
                        throw new Exception(exceptionMessages.get("durationInDays"));
                    if (depositBalance.compareTo(BigDecimal.ZERO) < 0)
                        throw new Exception(exceptionMessages.get("depositBalance"));
                    Deposit deposit = (Deposit) Class.forName("dotin.deposit." + depositType)
                            .getConstructor(String.class, BigDecimal.class, int.class)
                            .newInstance(customerNumber, depositBalance, durationInDays);
                    deposits.add(deposit);
                } catch (NullPointerException n) {
                    System.out.println(exceptionMessages.get("tags"));
                } catch (NumberFormatException nu) {
                    System.out.println(exceptionMessages.get("input"));
                } catch (ClassNotFoundException e) {
                    System.out.println(exceptionMessages.get("depositType"));
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }
        return deposits;
    }

    // ***************************************************************************************
    // This method get a list of Deposits,
    // sort them by payedInterest,
    // print and export customerNumber and payedInterest (PI) of every Deposit to a text file ...
    // ***************************************************************************************
    public void saveAndPrintPI(List<Deposit> deposits,String txtOutput) {
        try(FileWriter fileWriter = new FileWriter(txtOutput)) {
            InterestCalculatorImp interestCalculatorImp = InterestCalculatorImp.getInterestCalculatorImp();
            deposits.sort((o1, o2) -> interestCalculatorImp.calculateInterest(o2).
                    compareTo(interestCalculatorImp.calculateInterest(o1)));
            System.out.println("************************************************************************************");
            for (Deposit deposit : deposits) {
                fileWriter.write(deposit.getCustomerNumber() + " # " + interestCalculatorImp.calculateInterest(deposit) + "\n");
                System.out.println(deposit.getCustomerNumber() + " # " + interestCalculatorImp.calculateInterest(deposit));
            }
            System.out.println("************************************************************************************");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
