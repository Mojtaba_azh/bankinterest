/**
 * This class is used for defining custom messages for all exceptions (Singleton Pattern)
 *
 * @author Mojtaba Azhdari
 */
package dotin.utility;

import java.util.HashMap;
import java.util.Map;

public class ExceptionMessages {

    // **************************************************
    // Fields
    // **************************************************
    private static Map<String,String> messages;
    private static ExceptionMessages exceptionMessages = new ExceptionMessages();

    // **************************************************
    // private Constructor
    // **************************************************
    private ExceptionMessages() {
        messages = new HashMap<>();
        messages.put("customerNumber","ERROR-1 * WRONG CUSTOMER_NUMBER    :  The value of <customerNumber> should contain only digits (0-9) !");
        messages.put("durationInDays","ERROR-2 * WRONG DURATION_IN_DAYS   :  The value of <durationInDays> should be a positive integer number!");
        messages.put("depositBalance","ERROR-3 * WRONG DEPOSIT_BALANCE    :  The value of <depositBalance> can not be a negative number!");
        messages.put("tags",          "ERROR-4 * WRONG TAGS               :  Please declare all these tags in <deposit> in your XML file : <customerNumber> | <depositType> | <depositBalance> | <durationInDays>");
        messages.put("input",         "ERROR-5 * WRONG INPUT VALUES       :  Please make sure that declare and enter integer numbers for value of <customerNumber> and <durationInDays> and a positive number for <depositBalance>");
        messages.put("depositType",   "ERROR-6 * WRONG DEPOSIT_TYPE       :  Please change the value of <depositType> in  your XML file to one of these: Qarz | ShortTerm | LongTerm");
        messages.put("xmlFile",       "ERROR-7 * WRONG XML FILE           :  Your Input XML File does not exist.");
    }

    // **************************************************************************
    // This public method is used for getting instance of this class.
    // **************************************************************************
    public static ExceptionMessages getInstance() {
        return exceptionMessages;
    }

    // **************************************************
    // getter method for messages
    // **************************************************
    public Map<String, String> getExceptionMessages() {
        return messages;
    }
}
