/**
 * Entry point for this project.
 *
 * @author Mojtaba Azhdari
 */
package dotin;

import dotin.deposit.Deposit;
import dotin.utility.ExceptionMessages;
import dotin.utility.XmlUtility;
import java.io.*;
import java.util.List;

public class Runner {

    // **************************************************
    // Constants
    // **************************************************
    /** Contains path of input XML file used for XML Data Parsing, value is {@value #INPUT_XML_FILE}. */
    private static final String OUTPUT_TXT_FILE = "output.txt";

    /** Contains path of output txt file used for File Writer, value is {@value #OUTPUT_TXT_FILE}. */
    private static final String INPUT_XML_FILE = "deposit-info.xml";

    public static void main(String[] args) {
        List<Deposit> deposits;
        try {
            deposits = XmlUtility.getInstance().parse(INPUT_XML_FILE);
            XmlUtility.getInstance().saveAndPrintPI(deposits,OUTPUT_TXT_FILE);
        } catch (FileNotFoundException f) {
            System.out.println(ExceptionMessages.getInstance().getExceptionMessages().get("xmlFile"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
