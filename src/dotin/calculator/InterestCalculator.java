/**
 * This interface is used for defining calculateInterest method.
 *
 * @author Mojtaba Azhdari
 */
package dotin.calculator;

import dotin.deposit.Deposit;
import java.math.BigDecimal;

public interface InterestCalculator {

    // **************************************************************************
    // This is the definition of method for calculating payedInterest (PI) of different deposit types.
    // **************************************************************************
     BigDecimal calculateInterest(Deposit deposit);
}
