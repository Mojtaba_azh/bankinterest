/**
 * This class is used for implementing InterestCalculator interface
 *
 * @author Mojtaba Azhdari
 */
package dotin.calculator;

import dotin.deposit.Deposit;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class InterestCalculatorImp implements InterestCalculator {

    // **************************************************************************
    // This is the implementation of calculateInterest method of InterestCalculator Interface.
    // payedInterest = (interestRate * depositBalance * durationInDays) / 36500
    // **************************************************************************
    private static InterestCalculatorImp interestCalculatorImp = new InterestCalculatorImp();
    private InterestCalculatorImp() {
    }
    public static InterestCalculatorImp getInterestCalculatorImp() {
        return interestCalculatorImp;
    }

    @Override
    public BigDecimal calculateInterest(Deposit deposit) {
        BigDecimal interestRate = BigDecimal.valueOf(deposit.getInterestRate());
        BigDecimal depositBalance = deposit.getDepositBalance();
        BigDecimal durationInDays = BigDecimal.valueOf(deposit.getDurationInDays());

        return interestRate.multiply(depositBalance).multiply(durationInDays)
                .divide(BigDecimal.valueOf(36500),2, RoundingMode.HALF_UP);
    }
}
