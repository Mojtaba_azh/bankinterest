/**
 * This class is used for customers that have deposit without interest rate. (0 percent)
 *
 * @author Mojtaba Azhdari
 */
package dotin.deposit;

import java.math.BigDecimal;

public class Qarz extends Deposit {

    // **************************************************
    // Constructor
    // **************************************************
    public Qarz(String customerNumber, BigDecimal depositBalance, int durationInDays) {
        super(customerNumber, depositBalance, durationInDays);
        this.interestRate = 0;
    }
}
