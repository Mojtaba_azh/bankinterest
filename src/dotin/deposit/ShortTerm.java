/**
 * This class is used for customers that have ShortTerm deposit (10 percent interest rate.)
 *
 * @author Mojtaba Azhdari
 */
package dotin.deposit;

import java.math.BigDecimal;

public class ShortTerm extends Deposit  {

    // **************************************************
    // Constructor
    // **************************************************
    public ShortTerm(String customerNumber, BigDecimal depositBalance, int durationInDays) {
        super(customerNumber, depositBalance, durationInDays);
        this.interestRate = 10;
    }
}
