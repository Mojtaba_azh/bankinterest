/**
 * This abstract class define common fields and methods and used as a parent class for Qarz , ShortTerm and LongTerm classes.
 *
 * @author Mojtaba Azhdari
 */
package dotin.deposit;

import java.math.BigDecimal;

public abstract class Deposit {
    // **************************************************
    // Fields
    // **************************************************
    private final String customerNumber;
    private final BigDecimal depositBalance;
    private final int durationInDays;
    protected float interestRate;

    // **************************************************
    // Constructor
    // **************************************************
    /**
     * @param customerNumber first parameter of {@link #Deposit(String, BigDecimal, int)}.
     * @param depositBalance second parameter of {@link #Deposit(String, BigDecimal, int)}.
     * @param durationInDays third parameter of {@link #Deposit(String, BigDecimal, int)}.
    */
    public Deposit(String customerNumber, BigDecimal depositBalance, int durationInDays) {
        this.customerNumber = customerNumber;
        this.depositBalance = depositBalance;
        this.durationInDays = durationInDays;
    }

    // **************************************************
    // getter method for customerNumber
    // **************************************************
    public String getCustomerNumber() {
        return customerNumber;
    }

    public BigDecimal getDepositBalance() {
        return depositBalance;
    }

    public int getDurationInDays() {
        return durationInDays;
    }

    public float getInterestRate() {
        return interestRate;
    }
}
