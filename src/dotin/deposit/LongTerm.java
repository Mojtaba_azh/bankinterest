/**
 * This class is used for customers that have LongTerm deposit (20 percent interest rate.)
 *
 * @author Mojtaba Azhdari
 */
package dotin.deposit;

import java.math.BigDecimal;

public class LongTerm extends Deposit  {
    // **************************************************
    // Constructor
    // **************************************************
    public LongTerm(String customerNumber, BigDecimal depositBalance, int durationInDays) {
        super(customerNumber, depositBalance, durationInDays);
        this.interestRate = 20;
    }

}
